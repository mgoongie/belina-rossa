
document.addEventListener('mouseover', function (event) {

    if ( event.target.classList.contains( 'fas' ) ) {
        event.target.style.color = "orange";
    }
}, false);
document.addEventListener('mouseout', function (event) {
    if ( event.target.classList.contains( 'fas' ) ) {
        event.target.style.color = "grey";
    }
}, false);

(function ($) {
    $('#myCarousel').carousel({
        interval: 2000
    })
})();